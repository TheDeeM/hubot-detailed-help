// Description:
//   <description of the scripts functionality>
//
// Dependencies:
//   "<module name>": "<module version>"
//
// Configuration:
//   HELP_COMMAND_FORMAT - The format for displaying the command entry. The bot will automatically replace ${command} and ${description}
//   HELP_PARAMETER_FORMAT - The format for displaying the parameter entry. The bot will automatically replace ${parameter} and ${description}
//   HELP_TAG_FORMAT - The format for displaying the tag entry. The bot will automatically replace ${tag}, ${commands} (amount of commands) and ${description}
//   HELP_PARAMETERS_HEADER - The header of the parameters list summary.
//   HELP_ALIASES_HEADER - The header of the aliases list summary.
//   HUBOT_HELP_NOT_FOUND - Information displayed when the bot did not found any command matching user's query
//   HUBOT_COLLAPSE_HELP - If set to false, the bot will just simply show all commands (similiar to normal hubot-help behaviour). If set to true, the bot will by default show only tag groups.
//   HUBOT_HELP_TAGS_INFO - The summary for the tags list
//   HUBOT_HELP_UNTAGGED_INFO - The summary for the untagged commands list
//   HUBOT_HELP_HEADER - The main help header, used to inform user about the library's functionality
//   HUBOT_HELP_COLLAPSED_INFO - Information shown to user when the HUBOT_COLLAPSE_HELP is set to true
//
// Commands:
//   hubot help - Displays help [HELP]
//   hubot help `QUERY` - Searches help to find a command matching `QUERY`. {QUERY - The key to be found} [HELP]
//   TAG [HELP] - Commands used to display help
//
// Author:
//   Dorian Krefft <dorian.krefft@gmail.com>

'use strict';

const HelpLoader = require('../dist/help-loader').HelpLoader;

module.exports = (robot) => {
    robot.respond(/help$/i, (response) => {
        let responseParts = [getHeader()];

        const loader = new HelpLoader(robot);
        if(process.env.HUBOT_COLLAPSE_HELP === 'true'){
            responseParts.push(getCollapsedInfo());
            responseParts.push(displayShortHelp(loader));
        }
        else{
            responseParts.push(displayAllCommands(loader));
        }

        response.send(responseParts.join("\n"));
    });

    robot.respond(/help\s+(.+)$/i, (response) => {
        const query = response.match[1].trim();
        const loader = new HelpLoader(robot);
        const commands = loader.searchForCommand(query);
        if(commands.length === 0){
            const notFound = process.env.HUBOT_HELP_NOT_FOUND || 'Sorry, I did not find anything for the query: ';
            response.send(`${notFound}${query}`)
        }
        else if(commands.length === 1){
            response.send(commands[0].displayDetailed());
        }
        else{
            let result = [];
            for (const command of commands) {
                result.push(command.display());
            }
            response.send(result.join("\n"));
        }
    });

    function displayAllCommands(loader){
        let commands = [];
        for (const command of loader.commands) {
           commands.push(command.display());
        }
        return commands.join("\n");
    }

    function displayShortHelp(loader){
        let tags = [];
        for(const tag of loader.tags){
            tags.push(tag.display());
        }
        let untaggedCommands = [];
        for(const command of loader.commands){
            if(command.keywords.length === 0){
                untaggedCommands.push(command.display());
            }
        }
        const tagsInfo = process.env.HUBOT_HELP_TAGS_INFO || 'The list of all possible tags:';
        let result = [`${tagsInfo}\n${tags.join("\n")}`];
        if(untaggedCommands.length > 0){
            const untaggedInfo = process.env.HUBOT_HELP_UNTAGGED_INFO || 'The list of commands that have no tag:';
            result.push(`${untaggedInfo}\n${untaggedCommands.join("\n")}`)
        }
        return result.join("\n");
    }

    function getHeader(){
        return process.env.HUBOT_HELP_HEADER || (`
            Below you can find all of the commands that I know about. You can also narrow your search by adding some keywords - I will show you only those which you want to find. If only one command matches your query, I will display also more details about it :)
        `.trim())
    }

    function getCollapsedInfo(){
        return process.env.HUBOT_HELP_COLLAPSED_INFO || (`
            I've **grouped the similiar commands by their tags** to make the help easier to read. If you want to show commands from the group, just use help TAG_NAME.
        `.trim())
    }
};