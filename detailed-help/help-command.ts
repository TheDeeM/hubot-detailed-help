import {Helpers} from "./helpers";

export class HelpCommand {
    command: string;
    keywords: string[];
    description: string;
    parameters: any;
    aliases: string[];
    robot: any;
    robotName: string;

    constructor(helpString: string, robot: any) {
        this.robot = robot;
        this.robotName = robot.alias || robot.name;
        this.command = Helpers.parseCommand(helpString, this.robotName);
        this.description = Helpers.parseDescription(helpString, this.robotName);
        this.keywords = Helpers.parseKeywords(helpString);
        this.parameters = Helpers.parseParameters(helpString);
        this.aliases = Helpers.parseAliases(helpString, this.robotName);
    }

    matches(key: string): boolean {
        const keys = key.replace(/\s+/g, ' ').split(' ');
        const searches: string[] = [
            this.command, this.description,
            ...this.keywords, ...this.aliases
        ];
        return Helpers.findInArray(keys, searches)
    }

    isQuery(key: string): boolean {
        const command = Helpers.prepareToCompare(this.command);
        const query = Helpers.prepareToCompare(key);
        return command === query;
    }

    display() {
        const format: string = process.env.HELP_COMMAND_FORMAT || '_${command}_ - ${description}';
        return Helpers.format(format, {command: this.command, description: this.description});
    }

    displayDetailed() {
        const infos = [this.display(), this.prepareParametersInfo(), this.prepareAliasesInfo()].filter((el) => {
            return el != ''
        });
        return infos.join("\n");
    }

    private prepareParametersInfo(): string {
        const format: string = process.env.HELP_PARAMETER_FORMAT || '_${parameter}_ - ${description}';
        let parameters = [];
        for (const parameter of Object.keys(this.parameters)) {
            const param = Helpers.format(format, {parameter, description: this.parameters[parameter]});
            parameters.push(param);
        }
        const header = process.env.HELP_PARAMETERS_HEADER || 'Parameters list:';
        return parameters.length > 0 ? `${header}\n${parameters.join("\n")}` : '';
    }

    private prepareAliasesInfo(): string {
        const header = process.env.HELP_ALIASES_HEADER || 'You can also use this command with:';
        return this.aliases.length > 0 ? `${header}\n${this.aliases.join("\n")}` : '';
    }
}