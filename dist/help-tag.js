"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const helpers_1 = require("./helpers");
class HelpTag {
    constructor(tagString) {
        this.name = '';
        this.description = '';
        this.commands = [];
        if (tagString) {
            const finds = tagString.match(/^TAG\s+\[([^\[\]]*)]\s+[-:](.*)/);
            if (finds) {
                this.name = finds[1].trim();
                this.description = finds[2].trim();
            }
        }
    }
    display() {
        const format = process.env.HELP_TAG_FORMAT || '_${tag}_ (${commands}) - ${description}';
        return helpers_1.Helpers.format(format, { tag: this.name, description: this.description, commands: this.commands.length });
    }
}
exports.HelpTag = HelpTag;
