import { HelpCommand } from "./help-command";
import { HelpTag } from "./help-tag";
export declare class HelpLoader {
    commands: HelpCommand[];
    tags: HelpTag[];
    robot: any;
    constructor(robot: any);
    searchForCommand(key: string): HelpCommand[];
    private mapCommandsToTags;
    private findTagByName;
}
