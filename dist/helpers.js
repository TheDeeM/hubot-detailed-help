"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const accents = require("remove-accents");
class Helpers {
    static findInArray(keys, arr) {
        for (const key of keys) {
            const toFind = accents.remove(key.toLowerCase());
            let result = false;
            for (const str of arr) {
                const s = accents.remove(str.toLowerCase());
                if (s.includes(toFind)) {
                    result = true;
                    break;
                }
            }
            if (!result) {
                return false;
            }
        }
        return true;
    }
    static parseKeywords(helpString) {
        const finds = helpString.match(/\[[^\[\]]+]/g);
        if (finds) {
            let result = [];
            for (let i = 0; i < finds.length; i++) {
                const keyword = finds[i].match(/\[([^\[\]]+)]/) || ['', ''];
                if (result.indexOf(keyword[1]) < 0) {
                    result.push(unescape(keyword[1].trim()));
                }
            }
            return result;
        }
        return [];
    }
    static parseCommand(helpString, robotName) {
        const finds = Helpers.cleanCommand(helpString, robotName).match(/^(.*)\s+-/);
        if (finds) {
            return unescape(finds[finds.length - 1].trim());
        }
        return '';
    }
    static parseDescription(helpString, robotName) {
        const finds = Helpers.cleanCommand(helpString, robotName).match(/^.*\s+-(.*)/);
        if (finds) {
            return unescape(finds[finds.length - 1].trim());
        }
        return '';
    }
    static parseParameters(helpString) {
        const finds = helpString.match(/{[^{}]+\s+-\s+[^{}]+}/g);
        if (finds) {
            let result = {};
            for (let i = 0; i < finds.length; i++) {
                const param = finds[i].match(/{([^{}]+)\s+-\s+([^{}]+)}/) || ['', ''];
                result[unescape(param[1].trim())] = unescape(param[2].trim());
            }
            return result;
        }
        return {};
    }
    static parseAliases(helpString, robotName) {
        const finds = helpString.match(/<(?:Alias:)?\s*[^<>]+\s*>/g);
        if (finds) {
            let result = [];
            for (let i = 0; i < finds.length; i++) {
                const param = finds[i].match(/<(?:Alias:)?\s*([^<>]+)\s*>/) || [];
                const alias = unescape(param[1]).replace(/hubot/ig, robotName);
                result.push(alias);
            }
            return result;
        }
        return [];
    }
    static cleanCommand(command, robotName) {
        return command
            .replace(/<(?:Alias:)?\s*[^<>]+\s*>/g, '') // Remove aliases
            .replace(/\[[^\]\[]+]/g, '') // Remove tags
            .replace(/{[^{}]+\s+-\s+[^{}]+}/g, '') // Remove parameters
            .replace(/hubot/ig, robotName) // Use the bot's name
            .trim();
    }
    static prepareToCompare(str) {
        return str.toLowerCase()
            .replace(/\s+/g, ' ')
            .replace(/^hubot/ig, '')
            .replace(/[^a-zA-Z0-9 -]/, '')
            .trim();
    }
    static format(str, variables) {
        let result = str;
        for (const variable of Object.keys(variables)) {
            result = result.replace(new RegExp(`\\\${${variable}}`, 'g'), variables[variable]);
        }
        return result;
    }
}
exports.Helpers = Helpers;
