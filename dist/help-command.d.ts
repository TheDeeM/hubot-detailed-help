export declare class HelpCommand {
    command: string;
    keywords: string[];
    description: string;
    parameters: any;
    aliases: string[];
    robot: any;
    robotName: string;
    constructor(helpString: string, robot: any);
    matches(key: string): boolean;
    isQuery(key: string): boolean;
    display(): string;
    displayDetailed(): string;
    private prepareParametersInfo;
    private prepareAliasesInfo;
}
