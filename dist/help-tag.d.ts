import { HelpCommand } from "./help-command";
export declare class HelpTag {
    name: string;
    description: string;
    commands: HelpCommand[];
    constructor(tagString?: string);
    display(): string;
}
