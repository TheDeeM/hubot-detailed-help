// Description:
//   This is only to test parsing help commands
//
// Commands:
//   hubot some command - Performs some command [TEST] [COMMAND]
//   hubot some command `QUERY` - More complicated command {QUERY - The query for the command} [TEST] <hubot command `QUERY`>
//   hubot some untagged command  - Untagged command <hubot untagged>
//   TAG [TEST] - This tag has description
//   TAG [COMMAND] - This tag is used to display some commands
//
// Author:
//   Dorian Krefft <dorian.krefft@gmail.com>

'use strict';

module.exports = (robot) => {
};