// Description:
//   This is only to test parsing help commands
//
// Commands:
//   hubot some command - Performs some command [TEST] [COMMAND 1] [SPECIFIC]
//   hubot another command - Performs some command [TEST] [COMMAND 2]
//   hubot details of command 1 - Displays details of the command 1 [TEST] [COMMAND 1]
//   hubot untagged command - This command remains untagged because of some reason
//   TAG [TEST] - This tag has description
//   TAG [COMMAND 1] - This tag is used to display some commands
//
// Author:
//   Dorian Krefft <dorian.krefft@gmail.com>

'use strict';

module.exports = (robot) => {
};