// Description:
//   This is only to test parsing help commands
//
// Commands:
//   hubot this command does not have description -
//   some command - this command has empty tag name []
//   some command 2 - this command has empty parameter {}
//   some command 3 - this command has empty alias <>
//
// Author:
//   Dorian Krefft <dorian.krefft@gmail.com>

'use strict';

module.exports = (robot) => {
};