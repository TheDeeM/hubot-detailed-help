'use strict';
const expect = require('chai').expect;
const Helpers = require('../dist/helpers').Helpers;

describe('Testing the library helper function', () => {
    const robotName = 'bot';
    
    context('Parsing basic documentation', () => {
        it('should should be able to parse command from help string with the robot prefix', () => {
            const helpDoc = 'hubot help - Displays help';
            const command = Helpers.parseCommand(helpDoc, robotName);
            expect(command).to.eql('bot help');
        });

        it('should should be able to parse command from help string without the robot prefix', () => {
            const helpDoc = 'help - Displays help';
            const command = Helpers.parseCommand(helpDoc, robotName);
            expect(command).to.eql('help');
        });

        it('should should be able to parse command from help string when there is no description specified', () => {
            const helpDoc = 'help - ';
            const command = Helpers.parseCommand(helpDoc, robotName);
            expect(command).to.eql('help');
        });

        it('should should be able to parse command from help string when there are some dashes used in the command', () => {
            const helpDoc = 'hubot - detailed-help - - the command description';
            const command = Helpers.parseCommand(helpDoc, robotName);
            expect(command).to.eql('bot - detailed-help -');
        });

        it('should should be able to parse commands description from help string when it is provided', () => {
            const helpDoc = 'help - Displays help';
            const description = Helpers.parseDescription(helpDoc, robotName);
            expect(description).to.eql('Displays help');
        });

        it('should should not load any description when it is not provided', () => {
            const helpDoc = 'help - ';
            const description = Helpers.parseDescription(helpDoc, robotName);
            expect(description).to.be.empty;
        });
    });

    context('Parsing the keywords', () => {
        it('should not load any keywords when there are none provided', () => {
            const helpDoc = 'help - Displays help';
            const keywords = Helpers.parseKeywords(helpDoc);
            expect(keywords).to.be.empty;
        });

        it('should parse all keywords provided', () => {
            const helpDoc = 'help - Displays help [HELP] [ANOTHER]';
            const keywords = Helpers.parseKeywords(helpDoc);
            expect(keywords).to.be.deep.eql(['HELP', 'ANOTHER']);
        });

        it('should allow to use more complicated keywords', () => {
            const helpDoc = 'help - Displays help [THIS IS SOME LONG TAG]';
            const keywords = Helpers.parseKeywords(helpDoc);
            expect(keywords).to.be.deep.eql(['THIS IS SOME LONG TAG']);
        });

        it('should not create duplicated keywords', () => {
            const helpDoc = 'help - Displays help [HELP] [HELP]';
            const keywords = Helpers.parseKeywords(helpDoc);
            expect(keywords).to.be.deep.eql(['HELP']);
        });

        it('should not create invalid keyword', () => {
            const helpDoc = 'help - Displays help FIRST] [SECOND [[]';
            const keywords = Helpers.parseKeywords(helpDoc);
            expect(keywords).to.be.empty;
        });
    });

    context('Parsing parameters', () => {
        it('should not load any parameters when there are none provided', () => {
            const helpDoc = 'help - Displays help';
            const parameters = Helpers.parseParameters(helpDoc);
            expect(parameters).to.be.empty;
        });

        it('should load all provided parameters', () => {
            const helpDoc = 'help {QUERY} - Displays help {QUERY - The key to search}';
            const parameters = Helpers.parseParameters(helpDoc);
            expect(parameters).to.be.deep.eql({'QUERY': 'The key to search'});
        });

        it('should not load invalid parameters', () => {
            const helpDoc = 'help {QUERY} - Displays help {QUERY} {{} QUERY - description} {QUERY - description';
            const parameters = Helpers.parseParameters(helpDoc);
            expect(parameters).to.be.empty;
        });
    });
    
    context('Parsing aliases', () => {
        it('should not load any aliases when there are none provided', () => {
            const helpDoc = 'help - Displays help';
            const aliases = Helpers.parseAliases(helpDoc, 'hubot');
            expect(aliases).to.be.empty;
        });

        it('should load all provided aliases', () => {
            const helpDoc = 'help - Displays help <Alias: hubot help> <another command>';
            const aliases = Helpers.parseAliases(helpDoc, 'hubot');
            expect(aliases).to.be.deep.eql(['hubot help', 'another command']);
        });

        it('should not load any invalid aliases', () => {
            const helpDoc = 'help - Displays help Alias: command> <> > < <hubot help <Alias: another command ';
            const aliases = Helpers.parseAliases(helpDoc, 'hubot');
            expect(aliases).to.be.empty;
        });

        it('should update the robot name when it is different than hubot', () => {
            const helpDoc = 'help - Displays help <Alias: hubot help> <another command>';
            const aliases = Helpers.parseAliases(helpDoc, 'fancy-bot');
            expect(aliases).to.be.deep.eql(['fancy-bot help', 'another command']);
        });
    });
});