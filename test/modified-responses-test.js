'use strict';
const HubotChatTesting = require('hubot-chat-testing');
const Helper = require('hubot-test-helper');

describe('Testing the hubot help responses', () => {
    context('The configuration includes modified bot responses', () => {
        const chat = new HubotChatTesting('hubot', new Helper('../scripts/detailed-help.js'));

        chat.when('user is asking for help without collapsed commands')
            .setEnvironmentVariables({
                HUBOT_COLLAPSE_HELP: 'false'
            })
            .user('alice').messagesBot('help')
            .bot.messagesRoom(removeIndents(`
                Below you can find all of the commands that I know about. You can also narrow your search by adding some keywords - I will show you only those which you want to find. If only one command matches your query, I will display also more details about it :)
                _hubot help_ - Displays help
                _hubot help \`QUERY\`_ - Searches help to find a command matching \`QUERY\`.
            `))
            .expect('the bot should respond with the list of all help commands');

        chat.when('user is asking for help with collapsed commands')
            .setEnvironmentVariables({
                HUBOT_COLLAPSE_HELP: 'true'
            })
            .user('alice').messagesBot('help')
            .bot.messagesRoom(removeIndents(`
                Below you can find all of the commands that I know about. You can also narrow your search by adding some keywords - I will show you only those which you want to find. If only one command matches your query, I will display also more details about it :)
                I've **grouped the similiar commands by their tags** to make the help easier to read. If you want to show commands from the group, just use help TAG_NAME.
                The list of all possible tags:
                _HELP_ (2) - Commands used to display help
            `))
            .expect('the bot should respond with the list of all help commands');

        chat.when('user is asking for specific command')
            .setEnvironmentVariables({
                HUBOT_COLLAPSE_HELP: 'true'
            })
            .user('alice').messagesBot('help find help')
            .bot.messagesRoom(removeIndents(`
                _hubot help \`QUERY\`_ - Searches help to find a command matching \`QUERY\`.
                Parameters list:
                _QUERY_ - The key to be found
            `))
            .expect('the bot should show the details of this command');

        chat.when('there qre more commands matching one query')
            .setEnvironmentVariables({
                HUBOT_COLLAPSE_HELP: 'true'
            })
            .user('alice').messagesBot('help help')
            .bot.messagesRoom(removeIndents(`
               _hubot help_ - Displays help
               _hubot help \`QUERY\`_ - Searches help to find a command matching \`QUERY\`.
            `))
            .expect('the bot should show all the commands matching the query');

        chat.when('user is asking for a non-existing command')
            .setEnvironmentVariables({
                HUBOT_COLLAPSE_HELP: 'true'
            })
            .user('alice').messagesBot('help non-existing command')
            .bot.messagesRoom('Sorry, I did not find anything for the query: non-existing command')
            .expect('the bot should tell the user that it could not find anything matching the query');
    });
});

function removeIndents(string){
    return string.replace(/^ +/mg, "").trim();
}