'use strict';
const HubotChatTesting = require('hubot-chat-testing');
const Helper = require('hubot-test-helper');

describe('Testing the hubot help responses', () => {
    context('Tests for hubot-detailed-help library', () => {
        const chat = new HubotChatTesting('hubot', new Helper(['./test-scripts/invalid-help.js', '../scripts/detailed-help.js']));

        chat.when('user is asking for help that contains multiple tags')
            .setEnvironmentVariables({
                HUBOT_COLLAPSE_HELP: 'true'
            })
            .user('alice').messagesBot('help')
            .bot.messagesRoom(removeIndents(`
                Below you can find all of the commands that I know about. You can also narrow your search by adding some keywords - I will show you only those which you want to find. If only one command matches your query, I will display also more details about it :)
                I've **grouped the similiar commands by their tags** to make the help easier to read. If you want to show commands from the group, just use help TAG_NAME.
                The list of all possible tags:
                _HELP_ (2) - Commands used to display help
                The list of commands that have no tag:
                _hubot this command does not have description_ - 
                _some command_ - this command has empty tag name []
                _some command 2_ - this command has empty parameter {}
                _some command 3_ - this command has empty alias <>
            `))
            .expect('the bot should show the description for all tags');

        chat.when('user is asking the bot for the details about the command without description')
            .user('alice').messagesBot('help this command does not have description')
            .bot.messagesRoom('_hubot this command does not have description_ - ')
            .expect('the bot should show all details he can gather');

        chat.when('user is asking the bot for the details about the command without parameter name')
            .user('alice').messagesBot('help this command has empty parameter')
            .bot.messagesRoom('_some command 2_ - this command has empty parameter {}')
            .expect('the bot should show all details he can gather');

        chat.when('user is asking the bot for the details about the command without filled alias')
            .user('alice').messagesBot('help this command has empty alias')
            .bot.messagesRoom('_some command 3_ - this command has empty alias <>')
            .expect('the bot should show all details he can gather');
    });
});

function removeIndents(string){
    return string.replace(/^ +/mg, "").trim();
}