# hubot-detailed-help

A Hubot library to show more detailed help when user asks for the list of commands.

The main purpose of this library to make the help much easier for user to find interesting commands.
Each Hubot script should have the `Commands` section fulfilled - the more scripts you will add, 
the bigger the help will become. After some time you will find the it to be so overgrown, you would much
rather find the `Commands` section for the script by your own, to remove noise from other libraries. 

There are some already existing libraries with similiar functionality - I will try to explain, why 
this one was created.

## Other libraries

### hubot-help
The most common one is simple [hubot-help](https://www.npmjs.com/package/hubot-help). 
While this library is very good on it's own, it has disadvantages described above - 
it is too simple and allows to only display all help commands or search one using 
simple filtering. You can not add your tags, describing the parameters and adding
information about aliases will only make the help more complicated.

### hubot-advanced-help

The [hubot-advanced-help](https://www.npmjs.com/package/hubot-advanced-help) library is much more powerful
than the default one. You can add tags to commands and choose between AND/OR filtering. The biggest problem
with the library is that you have to manually include script paths for each script - it modified the design
of the `Commands` section, what caused it to be not compatible with existing mechanism.

## Functionality

The `hubot-detailed-help` library allows you to:
1. Group the commands by the tags
1. Add the description for the parameters used in the command
1. Add aliases for the command (when there two or more commands that are doing the same thing)

### The flow of using the help
Help is divided into three possible scenarios:
1. Summary of the help - displays all the commands or tags defined in scripts
1. Searching for the query and there are multiple matching commands - displays the `command` - `description` summary for
each command matching the query.
1. Searching for the query and there is only one matching command - displays the `command` and `description` summary,
but will also show list of all parameters (with description) and aliases (if there is any).

### Collapsing the help using the tag groups

By default, when the user types `hubot help`, the bot will show all commands with only their descriptions.
If there are many commands, the help will become unreadable. To avoid the problem, there is "collapsing mechanism" 
implemented in the library.

If the `HUBOT_COLLAPSE_HELP` environment variable will be set to `true`, the mechanism will collapse all the commands
into the groups displaying only the tags (and untagged commands). 

Lets use an example. When the variable will not be set, the help will show something like:
```text
 Below you can find all of the commands that I know about. You can also narrow your search by adding some keywords - I will show you only those which you want to find. If only one command matches your query, I will display also more details about it :)
_hubot help_ - Displays help
_hubot help `QUERY`_ - Searches help to find a command matching `QUERY`.
_hubot some command `QUERY`_ - More complicated command
_hubot some untagged command_  - This command remains untagged because of some reason
```

If we will change the variable to `true`, the bot will show instead:
```text
Below you can find all of the commands that I know about. You can also narrow your search by adding some keywords - I will show you only those which you want to find. If only one command matches your query, I will display also more details about it :)
I've **grouped the similiar commands by their tags** to make the help easier to read. If you want to show commands from the group, just use help TAG_NAME.
The list of all possible tags:
_HELP_ (2) - Commands used to display help
_EXTERNAL-SCRIPT_ (1) - Commands from the external script
The list of commands that have no tag:
_hubot some untagged command_ - This command remains untagged because of some reason
```

If the user wants to show the commands from the tag, he should type:
```text
hubot help EXTERNAL-SCRIPT
```

### Displaying the command details

This is the most important functionality of the library - after all, it is named `hubot-detailed-help`.
The details provided in the help will not be shown until the user will want to show information about the
specific command (in other words, only one command will match the query). This will prevent the help from being too chaotic
to find anything useful.

The details, apart from the command and description, will also display the parameters and aliases list, for example:
```text
_hubot find `ENGINE` `QUERY`_ - Searches the search engine to find any URLs matching query.
Parameters list:
_ENGINE_ - The name of the search engine that will be used to find URLs. Optional, defaults to Google.
_QUERY_ - The string that will be searched
You can also use this command with:
hubot search `ENGINE` `QUERY`
hubot search `QUERY`
hubot find `QUERY`
```
If there are no parameters or aliases defined, the sections will be simply skipped.

### Creating the help entries with the hubot-detailed-help library

This library does not modify the main design of the `Commands` section. 
Each entry should still contain `command` - `description` pattern - you can add more details by simply extending 
the description part. This way if somebody will still use simple `hubot-help`, your scripts will work without 
any problems (just displaying much more information, which you would add this or another way).

Each extension can be added in any place of the description. Just remember that each command should take exactly 
one line - there is no possibility to split the commands in separate lines.

#### Parameters

Parameters can be defined using the `{PARAM - DESCRIPTION}` syntax, for example: `{QUERY - The key to be found}`.

#### Aliases

Aliases can be defined using the `<Alias: ALIAS>` or simply `<ALIAS>` syntax, for example: `<hubot find> <Alias: hubot search>`.

#### Tags

Tags can be defined by using the `[]`, for example: `[TAG-NAME] [ANOTHER-TAG]`. 
Tags can not be described in command line - this is to avoid too many copy-paste of the same description everywhere.
Instead, you have to add special entry for each tag as it would be separated command, using the pattern:
```text
TAG [HELP] - Commands used to display help
```
This way you only have to define the description once and then just use your tag name inside command definitions.
Tags without the entry will just have empty description (we recommend adding it for every tag).

User can search by the tag even if it will not be displayed.

### Changing the entries format
You can change the way the parameters, tags and commands are displayed by changing the environment variables. The bot will
replace the parameters defined as `${parameter}` with it's proper value.

| Variable               | Default value                               | Description                                                                                                                  |
| ---------------------- | ------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| HELP_COMMAND_FORMAT    | _${command}_ - ${description}               | The format for displaying the command entry.                                                                                 |
| HELP_PARAMETER_FORMAT  | _${parameter}_ - ${description}             | The format for displaying the parameter entry                                                                                |
| HELP_TAG_FORMAT        | _${tag}_ (${commands}) - ${description}     | The format for displaying the tag entry. `commands` variable contains the amount of commands stored inside the group.        |

### Changing the bot responses

If you want to change the language of the help or just simply do not like bot's default responses, you can change them
by modifying proper environment variables.

| Variable                   | Description                                                                                                               |
| -------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| HUBOT_HELP_HEADER          | The main help header, used to inform user about the library's functionality. Displayed only when user types `hubot help`. |
| HUBOT_HELP_COLLAPSED_INFO  | Information shown to user when the HUBOT_COLLAPSE_HELP is set to true. Displayed only when user types `hubot help`.       |
| HUBOT_HELP_TAGS_INFO       | The header for the tags list. Displayed only when the help is opened in collapse mode.                                    |
| HUBOT_HELP_UNTAGGED_INFO   | The header for the untagged commands list. Displayed only when the help is opened in collapse mode.                       |
| HUBOT_HELP_NOT_FOUND       | Information displayed when the bot did not found any command matching user's query.                                       |
| HELP_ALIASES_HEADER        | The header of the aliases list summary. Displayed only in command's details mode.                                         |
| HELP_PARAMETERS_HEADER     | The header of the parameters list summary. Displayed only in command's details mode.                                      |


### Examples
Best example would be the `hubot-detailed-help` 
[Hubot script](https://gitlab.com/hubot-scripts/hubot-detailed-help/blob/master/scripts/detailed-help.js#L20).
If you want to see more examples, please use the [tests](https://gitlab.com/TheDeeM/hubot-detailed-help/tree/master/test/test-scripts)
as extended documentation.

## Contribution
If you want to contribute, just fork the project and add some changes!

## Errors and ideas to improve
If you will find any errors with this library or have great idea how to improve it (and don't want to do it on your own),
please feel free to open [a new ticket](https://gitlab.com/hubot-scripts/hubot-detailed-help/issues). 
